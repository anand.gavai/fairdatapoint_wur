# FairDataPoint

## Use the docker compose file start the deamon

``` docker-compose up ```


## To stop the deamon

``` docker-compose down ```


## FDP editor runs on port 8001 from a browser go to 

``` http://localhost:8001 ```

## The editor communicates with server at port 8003

``` http://localhost:8003/fdp ```

## As of now the settings.json file from config directory is not getting copied do the following:
## In the running container copy this file like

``` docker cp ./config/editor/settings.json editor:usr/share/nginx/html/conf/ ```

## Stop the deamon using 
```docker-compose down ``` 
and start it again using 

```docker-compose up``` 


## Make sure to use your own absolute path to your home directory when you build this image in the docker-compose.yml !  

