# Common prefixes

PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX obo: <http://purl.obolibrary.org/obo/> 
PREFIX dcterms: <http://purl.org/dc/terms/>
PREFIX sosa: <http://www.w3.org/ns/sosa/>
PREFIX efo: <http://www.ebi.ac.uk/efo/>
PREFIX co: <http://www.cropontology.org/rdf/>
PREFIX agrovoc: <http://aims.fao.org/aos/agrovoc/>
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX gn: <http://www.geonames.org/ontology#>

start = @<#sample>

<#sample>{
	rdf:type [obo:OBI_0100051] ;
	dcterms:identifier LITERAL ;
    sosa:resultTime xsd:dateTime ;
    obo:RO_0000086 @<#LOQ> ; # RO_0000086 = has quality
    obo:RO_0000086 @<#ZON> ;
}

<#LOQ> {
	rdf:type  [co:Measurement] ;
	obo:RO_0000086  [obo:CHMO_0002802] ; # RO_0000086 = has quality ;CHMO_0002802 = limit of quantification
	rdf:value xsd:integer ;
	obo:RO_0002536  [efo:EFO_0002899] ;  # RO_0002536 = measurement property has unit ; EFO_0002899 = microgram per kilogram	 
}

<#ZON> {
	rdf:type  [co:Measurement] ;
	rdf:value xsd:integer ;
	obo:RO_0002536  [efo:EFO_0002899] ;  # RO_0002536 = measurement property has unit ; EFO_0002899 = microgram per kilogram	 
	obo:RO_0002607  [obo:CHEBI_10106] ;  # RO_0002607 = is marker for ; CHEBI_10106 = zearalenone
}

<#cultivar>{
	rdf:type    efo:EFO_0005136 ;
	obo:RO_0002462  @<#crop> ; # RO_0002462 = "subject participant in"
}

<#crop> {
	rdf:type  [obo:AGRO_0000032] ;
	sosa:hasSample @<#sample> ;
	gn:locatedIn @<#parcel> ;
	obo:RO_0002462  @<#growseason> ; # RO_0002462 = "subject participant in"
	obo:RO_0002162  @<#cultivar> ; # RO_0002162 = "in taxon"
}

<#growseason>{
	rdf:type   obo:AGRO_01000020 ;
	obo:RO_0002537	xsd:dateTime ; # RO_0002537 = "has start time value"
	dcterms:date  { 
		obo:RO_0002546	obo:AGRO_00000142 ; # AGRO_00000142 = "seeding"; ; RO_0002546 = "has developmental stage marker"
		rdf:value xsd:dateTime ;
	} ;
	dcterms:date  { 
		obo:RO_0002546  obo:PO_0009046 ; # PO_0009046 = "flower"; RO_0002546 = "has developmental stage marker"
		rdf:value xsd:dateTime ;
	} ;
}


<#parcel>{
	rdf:type    [agrovoc:c_2ee25bd9] ;
	dcterms:isPartOf @<#farm> ;
	dcterms:hasPart @<#soil> ;
	obo:RO_0001019  @<#soil_types> ; # RO_0001019 = contains
	obo:AGRO_00000429 @<#tillage> ; # AGRO_00000429 = hasTreatment
	obo:AGRO_00000429 @<#spraying>? ; # AGRO_00000429 = hasTreatment
}

<#soil_types> [
	obo:ENVO_00002982 # Klei
	obo:ENVO_01000017 # Zand
	wd:Q965254 # Zavel TODO silt
]

<#tillage> [
	obo:AGRO_0000002 # No-till
	obo:AGRO_00000029 # Mulch-till = ploegen
	obo:AGRO_00000031 # strip-till = spitten
]

<#spraying> {
	rdf:type obo:AGRO_00000031  ;# chemical pest control process
	obo:RO_0002248 obo:CHEBI_24127  ;  # RO_0002248 = has active ingredient ; CHEBI:24127 = fungicide
}


<#farm> {
	rdf:type   	[obo:RO_0001019] ;
}


<#soil>{
	rdf:type  agrovoc:c_7156 ; # soil according to agrovoc 
	dcterms:hasPart @<#soil_types> ;
}
