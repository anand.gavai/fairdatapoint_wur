from rdflib import Namespace, Graph, URIRef, BNode, Literal
from rdflib.namespace import DCTERMS, RDFS, RDF, DC, XSD
import pandas as pd
import uuid

rikilt_graph = Graph()
rikilt_graph.bind("sosa", "http://www.w3.org/ns/sosa/")
rikilt_graph.bind("dcterms","http://purl.org/dc/terms/")
rikilt_graph.bind("wikidata", "http://www.wikidata.org/entity/")
rikilt_graph.bind("obo", "http://purl.obolibrary.org/obo/")
rikilt_graph.bind("efo", "http://www.ebi.ac.uk/efo/")
rikilt_graph.bind("co", "http://www.cropontology.org/rdf/")
rikilt_graph.bind("geo", "http://www.w3.org/2003/01/geo/wgs84_pos#")
rikilt_graph.bind("agrovoc", "http://aims.fao.org/aos/agrovoc/")
rikilt_graph.bind("gn", "http://www.geonames.org/ontology#")

# NAMESPACES
SOSA = Namespace("http://www.w3.org/ns/sosa/")
WD = Namespace("http://www.wikidata.org/entity/")
OBO = Namespace("http://purl.obolibrary.org/obo/")
CO = Namespace("http://www.cropontology.org/rdf/")
EFO = Namespace("http://www.ebi.ac.uk/efo/")
GEO = Namespace("http://www.w3.org/2003/01/geo/wgs84_pos#")
AGROVOC = Namespace("http://aims.fao.org/aos/agrovoc/")
GN = Namespace("http://www.geonames.org/ontology#")

# Used URIs
rikiltbase = "http://sample.rikilt.nl/FAIR/wheatstudy/"


df = pd.read_excel(io="2016_2017 ZEA wheat.xlsx", sheet_name="ZEA wheat 2016 2017")
print(df.head(5))  # print first 5 rows of the dataframe

# Dutch provinces
prov = dict()
prov["Zuid-Holland"] = WD.Q694
rikilt_graph.add((WD.Q694, RDFS.label, Literal("Zuid-Holland", lang="nl")))
prov["Noord-Holland"] = WD.Q701
rikilt_graph.add((WD.Q701, RDFS.label, Literal("Noord-Holland", lang="nl")))
prov["Zeeland"] = WD.Q705
rikilt_graph.add((WD.Q705, RDFS.label, Literal("Zeeland", lang="nl")))
prov["Flevoland"] = WD.Q707
rikilt_graph.add((WD.Q707, RDFS.label, Literal("Flevoland", lang="nl")))
prov["Groningen"] = WD.Q752
rikilt_graph.add((WD.Q752, RDFS.label, Literal("Groningen", lang="nl")))
prov["Friesland"] = WD.Q770
rikilt_graph.add((WD.Q770, RDFS.label, Literal("Friesland", lang="nl")))
prov["Drenthe"] = WD.Q772
rikilt_graph.add((WD.Q772, RDFS.label, Literal("Drenthe", lang="nl")))
prov["Overijssel"] = WD.Q773
rikilt_graph.add((WD.Q773, RDFS.label, Literal("Overijssel", lang="nl")))
prov["Gelderland"] = WD.Q775
rikilt_graph.add((WD.Q775, RDFS.label, Literal("Gelderland", lang="nl")))
prov["Utrecht"] = WD.Q776
rikilt_graph.add((WD.Q7776, RDFS.label, Literal("Utrecht", lang="nl")))
prov["Limburg"] = WD.Q1093
rikilt_graph.add((WD.Q1093, RDFS.label, Literal("Limburg", lang="nl")))
prov["Noord-Brabant"] = WD.Q1101
rikilt_graph.add((WD.Q1101, RDFS.label, Literal("Noord-Brabant", lang="nl")))

soil_types = dict()
soil_types["klei"] = OBO.ENVO_00002982
rikilt_graph.add((OBO.ENVO_00002982, RDFS.label, Literal("klei", lang="nl")))
soil_types["zand"] = OBO.ENVO_01000017
rikilt_graph.add((OBO.ENVO_01000017, RDFS.label, Literal("zand", lang="nl")))
soil_types["zavel"] = WD.Q965254
rikilt_graph.add((WD.Q965254, RDFS.label, Literal("zavel", lang="nl")))


cultivar_purl = dict()
cultivar_purl["Hendrik"] = URIRef("https://purl.org/germplasm/id/042c08c8-1238-4dae-aebe-a6779bd7aa1d")
rikilt_graph.add((cultivar_purl["Hendrik"], RDFS.label, Literal("Hendrik", lang="en")))


for index, row in df.iterrows():
    print(row["Unieke_code"])
    sampleURI = URIRef(rikiltbase+row["Unieke_code"])

    # Sample
    rikilt_graph.add((sampleURI, RDF.type, URIRef("http://purl.obolibrary.org/obo/OBI_0100051")))
    rikilt_graph.add((sampleURI, DCTERMS.identifier, Literal(row["Unieke_code"])))
    rikilt_graph.add((sampleURI, SOSA.resultTime, Literal(str(row["year"])+"-00-00T00:00:00Z", datatype=XSD.dateTime)))

    # LOQ In this model LOQ and ZON are not related is this correct?
    measurementURI = BNode(value=str(uuid.uuid4()))

    rikilt_graph.add((sampleURI, OBO.RO_0000086, measurementURI))
    rikilt_graph.add((measurementURI, RDF.type, CO.Measurement))
    rikilt_graph.add((measurementURI, OBO.RO_0000086, OBO.CHMO_0002802)) # RO_0000086 = has quality
    rikilt_graph.add((OBO.CHMO_0002802, RDFS.label, Literal("limit of quantification", lang="en")))
    rikilt_graph.add((measurementURI, RDF.value, Literal(row["LOQ"], datatype=XSD.integer)))
    rikilt_graph.add((measurementURI, OBO.RO_0002536, EFO.EFO_0002899))
    rikilt_graph.add((OBO.RO_0002536, RDFS.label, Literal("measurement property has unit", lang="en")))
    rikilt_graph.add((EFO.EFO_0002899, RDFS.label, Literal("microgram per kilogram", lang="en")))

    # ZON
    measurementURI = BNode(value=str(uuid.uuid4()))
    rikilt_graph.add((sampleURI, OBO.RO_0000086, measurementURI))
    rikilt_graph.add((measurementURI, RDF.type, CO.Measurement))
    rikilt_graph.add((OBO.RO_0000086, RDFS.label, Literal("has quality", lang="en")))
    rikilt_graph.add((measurementURI, RDF.value, Literal(row["ZON"], datatype=XSD.integer)))
    rikilt_graph.add((measurementURI, OBO.RO_0002536, EFO.EFO_0002899))
    rikilt_graph.add((measurementURI, OBO.RO_0002607, OBO.CHEBI_10106))
    rikilt_graph.add((OBO.RO_0002607, RDFS.label, Literal("is marker for", lang="en")))
    rikilt_graph.add((OBO.CHEBI_10106, RDFS.label, Literal("zearalenone", lang="en")))
    rikilt_graph.add((OBO.RO_0002536, RDFS.label, Literal("measurement property has unit", lang="en")))
    rikilt_graph.add((EFO.EFO_0002899, RDFS.label, Literal("microgram per kilogram", lang="en")))


    # Model crop
    #crop
    crop = BNode(value=uuid.uuid4())
    rikilt_graph.add((crop, RDF.type, OBO.AGRO_00000325))
    rikilt_graph.add((OBO.AGRO_00000325, RDFS.label, Literal("crop", lang="en")))
    rikilt_graph.add((crop, SOSA.hasSample, sampleURI))


    # Model growing season
    growseasonURI = BNode(value=row["Unieke_code"])
    rikilt_graph.add((crop, OBO.RO_0002462, growseasonURI))
    rikilt_graph.add((OBO.RO_0002462, RDFS.label, Literal("subject participant in", lang="en")))
    rikilt_graph.add((growseasonURI, RDF.type, OBO.AGRO_01000020))
    rikilt_graph.add((OBO.AGRO_01000020, RDFS.label, Literal("crop cultivation process", lang="nl")))
    rikilt_graph.add((growseasonURI, OBO.RO_0002537, Literal(row["sowing_date"], datatype=XSD.dateTime)))
    rikilt_graph.add((OBO.RO_0002537, RDFS.label, Literal("has start time value", lang="en")))
    rikilt_graph.add((sampleURI, OBO.RO_0002507, growseasonURI)) ## Again Fuzzy decision only applicable for this poc.

    #Sowing date
    sowingdate = BNode(value=str(uuid.uuid4()))
    rikilt_graph.add((sowingdate, OBO.RO_0002546, OBO.AGRO_00000142))
    rikilt_graph.add((sowingdate, RDF.value, Literal(Literal(row["sowing_date"], datatype = XSD.dateTime))))
    rikilt_graph.add((growseasonURI, DCTERMS.date, sowingdate))

    # Flowering date
    floweringdate = BNode(value=str(uuid.uuid4()))
    rikilt_graph.add((floweringdate, OBO.RO_0002546, OBO.PO_0009046))
    rikilt_graph.add((floweringdate, RDF.value, Literal(Literal(row["sowing_date"], datatype=XSD.dateTime))))
    rikilt_graph.add((growseasonURI, DCTERMS.date, floweringdate))

    # cultivar
    if row["cultivar"] in cultivar_purl.keys():
        cultivar = cultivar_purl[row["cultivar"]] # If cultivar in Genesys use that purl providing additional knowledge
    else:
        cultivar = BNode(value=str(uuid.uuid4()))
        rikilt_graph.add((cultivar, RDF.type, EFO.EFO_0005136))
        rikilt_graph.add((EFO.EFO_0005136, RDFS.label, Literal("Cultivar", lang="en")))

    rikilt_graph.add((crop, OBO.RO_0002162, cultivar))

    # FARM
    farm = BNode(value=str(uuid.uuid4()))
    rikilt_graph.add((farm, RDF.type, WD.Q131596))
    rikilt_graph.add((WD.Q131596, RDFS.label, Literal("farm", lang="en")))
    # rikilt_graph.add((farm, GEO.lat, Literal()))
    # rikilt_graph.add((farm, GEO.lon, Literal()))
    rikilt_graph.add((farm, GEO.location, prov[row["Provincie"]]))

    # Parcel
    parcel = BNode(value=str(uuid.uuid4()))
    rikilt_graph.add((crop, GN.locatedIn, parcel))
    rikilt_graph.add((growseasonURI, OBO.RO_0002462, parcel)) #TODO add rdfs:label ALSO multiple parcel
    rikilt_graph.add((parcel, DCTERMS.isPartOf, farm))
    rikilt_graph.add((parcel, RDF.type, AGROVOC.c_2ee25bd9))
    rikilt_graph.add((AGROVOC.c_2ee25bd9, RDFS.label, Literal("parcel plan", lang="en")))
    rikilt_graph.add((parcel, DCTERMS.isPartOf, farm))
    if isinstance(row["soil_type"], str):
        rikilt_graph.add((parcel, OBO.RO_0001019, soil_types[row["soil_type"].lower()]))
    rikilt_graph.add((OBO.RO_0001019, RDFS.label, Literal("contains", lang="en")))

    if row["Ploughing_yn"] == 0:
        treatment = BNode(value=uuid.uuid4())
        rikilt_graph.add((parcel, OBO.AGRO_00000429, treatment))
        rikilt_graph.add((treatment, RDF.type , OBO.AGRO_00000026))
        rikilt_graph.add((OBO.AGRO_00000429, RDFS.label, Literal("hasTreatment", lang="en")))
        rikilt_graph.add((OBO.AGRO_00000026, RDFS.label, Literal("no-till", lang="en")))
    elif row["Tillage"] == "ploegen":
        treatment = BNode(value=uuid.uuid4())
        rikilt_graph.add((parcel, OBO.AGRO_00000429, treatment))
        rikilt_graph.add((treatment, OBO.AGRO_00000429, OBO.AGRO_00000029))
        rikilt_graph.add((OBO.AGRO_00000429, RDFS.label, Literal("hasTreatment", lang="en")))
        rikilt_graph.add((OBO.AGRO_00000031, RDFS.label,
                              Literal("mulch-till", lang="en")))  # Arbitrary translate "ploegen in mulch till
    elif row["Tillage"] == "spitten":
        treatment = BNode(value=uuid.uuid4())
        rikilt_graph.add((parcel, OBO.AGRO_00000429, treatment))
        rikilt_graph.add((treatment, OBO.AGRO_00000429, OBO.AGRO_00000031))
        rikilt_graph.add((OBO.AGRO_00000429, RDFS.label, Literal("hasTreatment", lang="en")))
        rikilt_graph.add((OBO.AGRO_00000031, RDFS.label, Literal("strip-till", lang="en"))) # Arbitrary translate "spiten in strip till

    # Spraying
    if row["Spray_yn"] == 1:
        treatment = BNode(value=uuid.uuid4())
        rikilt_graph.add((parcel, OBO.AGRO_00000429, treatment))
        rikilt_graph.add((treatment, OBO.AGRO_00000429, OBO.AGRO_00000024))
        rikilt_graph.add((OBO.AGRO_00000429, RDFS.label, Literal("hasTreatment", lang="en")))
        rikilt_graph.add((OBO.AGRO_00000031, RDFS.label, Literal("chemical pest control process", lang="en")))
        rikilt_graph.add((treatment, DC.accrualPeriodicity, Literal(row["Spray_freq"])))
        rikilt_graph.add((treatment, OBO.RO_0002248, OBO.CHEBI_24127))



    # rikilt_graph.add((parcel, GEO.lat, Literal()))
    # rikilt_graph.add((parcel, GEO.lon, Literal()))
    # rikilt_graph.add((parce))






    # Dutch Provinces
    # Netherlands = CO_705.0000528



rikilt_graph.serialize(destination='Wheat.ttl', format='turtle')