#!/usr/bin/perl -w
use strict;

my @prefix = (
    '@prefix taxncbi: <http://www.ncbi.nlm.nih.gov/taxonomy/> .',
    '@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .',
    '@prefix foaf: <http://xmlns.com/foaf/0.1/> .',
    );

for (@prefix) {
    print "$_\n";
}

my $produces = "<http://purl.obolibrary.org/obo/RO_0003000>";

my %seen;
for (<>) {
    s/\R//g;
    my @tab = split/,/;
    my ($spid, $sp, $toxins, $spc, $spcid) = @tab;
    print "taxncbi:$spid a <http://www.cropontology.org/rdf/CO_020:0000006> .\n";
    print "taxncbi:$spid  rdfs:label  \"$sp\".\n";
    print "taxncbi:$spcid foaf:member taxncbi:$spid .\n";
    unless ($seen{$spcid}) {
	$seen{$spcid}++;
	print "taxncbi:$spcid rdfs:label \"$spc\" .\n";
    }
    for (split/;/, $toxins) {
	if ($_ eq "(deoxy)nivalenol") {
	    print "taxncbi:$spid $produces  <https://pubchem.ncbi.nlm.nih.gov/compound/440908> .\n";
	    print "taxncbi:$spid $produces  <https://pubchem.ncbi.nlm.nih.gov/compound/102515038> .\n";
	    print "taxncbi:$spid $produces  <https://pubchem.ncbi.nlm.nih.gov/compound/44715214> .\n";
	}
	print "taxncbi:$spid $produces  <https://pubchem.ncbi.nlm.nih.gov/compound/5281576> .\n" if /Zearalenone/;
	print "taxncbi:$spid $produces  <https://pubchem.ncbi.nlm.nih.gov/compound/62314> .\n" if /Fumonisin/;
	if ($_ eq "(H)T-2 toxin") {
	    print "taxncbi:$spid $produces  <https://pubchem.ncbi.nlm.nih.gov/compound/442400> .\n";
	    print "taxncbi:$spid $produces  <https://pubchem.ncbi.nlm.nih.gov/compound/10093830> .\n";
	}
    }	
    
}
for my $toxin (440908, 102515038, 44715214, 5281576, 62314, 442400, 10093830) {
    print "<https://pubchem.ncbi.nlm.nih.gov/compound/$toxin> a <http://purl.obolibrary.org/obo/CHEBI_25442> .\n";
}
